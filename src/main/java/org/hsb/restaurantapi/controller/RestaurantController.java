package org.hsb.restaurantapi.controller;

import lombok.RequiredArgsConstructor;
import org.hsb.restaurantapi.entity.Member;
import org.hsb.restaurantapi.model.restaurant.RestaurantInfoChangeRequest;
import org.hsb.restaurantapi.model.restaurant.RestaurantItem;
import org.hsb.restaurantapi.model.restaurant.RestaurantCreateRequest;
import org.hsb.restaurantapi.model.restaurant.RestaurantResponse;
import org.hsb.restaurantapi.service.MemberService;
import org.hsb.restaurantapi.service.RestaurantService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/restaurant")
public class RestaurantController {
    private final MemberService memberService;
    private final RestaurantService restaurantService;

    @PostMapping("/member-id/{memberId}")
    public String setRestaurant(@PathVariable long memberId, @RequestBody RestaurantCreateRequest request){
        Member member = memberService.getData(memberId);
        restaurantService.setRestaurant(member, request);

        return "OK";
    }

    @GetMapping("/all")
    public List<RestaurantItem> getRestaurants(){
        return restaurantService.getRestaurants();
    }

    @GetMapping("/detail/[id}")
    public RestaurantResponse getRestaurant(@PathVariable long id){
        return restaurantService.getRestaurant(id);
    }

    @PutMapping("/restaurant-info/{id}")
    public String putRestaurant(@PathVariable long id, @RequestBody RestaurantInfoChangeRequest request){
        restaurantService.putRestaurant(id, request);

        return "OK";
    }

    @DeleteMapping("/{id}")
    public String delRestaurant(@PathVariable long id){
        restaurantService.delRestaurant(id);

        return "OK";
    }
}

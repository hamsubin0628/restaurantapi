package org.hsb.restaurantapi.controller;

import lombok.RequiredArgsConstructor;
import org.hsb.restaurantapi.model.member.MemberItem;
import org.hsb.restaurantapi.model.member.MemberCreateRequest;
import org.hsb.restaurantapi.model.member.MemberResponse;
import org.hsb.restaurantapi.service.MemberService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/member-info")
    public String putMember(@RequestBody MemberCreateRequest request){
        memberService.setMember(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers(){
        return memberService.getMembers();
    }

    @GetMapping("/detail/{id}")
    public MemberResponse getMember(@PathVariable long id){
        return memberService.getMember(id);
    }
}

package org.hsb.restaurantapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Restaurant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false, length = 20)
    private String recommend;

    @Column(nullable = false, length = 50)
    private String address;

    @Column(nullable = false, length = 13)
    private String restaurantNumber;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}

package org.hsb.restaurantapi.service;

import lombok.RequiredArgsConstructor;
import org.hsb.restaurantapi.entity.Member;
import org.hsb.restaurantapi.entity.Restaurant;
import org.hsb.restaurantapi.model.restaurant.RestaurantInfoChangeRequest;
import org.hsb.restaurantapi.model.restaurant.RestaurantItem;
import org.hsb.restaurantapi.model.restaurant.RestaurantCreateRequest;
import org.hsb.restaurantapi.model.restaurant.RestaurantResponse;
import org.hsb.restaurantapi.repository.RestaurantRepository;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RestaurantService {
    private final RestaurantRepository restaurantRepository;

    public void setRestaurant(Member member, RestaurantCreateRequest request){
        Restaurant addData = new Restaurant();
        addData.setMember(member);
        addData.setRecommend(request.getRecommend());
        addData.setAddress(request.getAddress());
        addData.setRestaurantNumber(request.getRestaurantNumber());
        addData.setEtcMemo(request.getEtcMemo());

        restaurantRepository.save(addData);
    }

    public List<RestaurantItem> getRestaurants(){
        List<Restaurant> originData = restaurantRepository.findAll();
        List<RestaurantItem> result = new LinkedList<>();

        for (Restaurant restaurant : originData){
            RestaurantItem addItem = new RestaurantItem();
            addItem.setId(restaurant.getId());
            addItem.setMemberId(restaurant.getMember().getId());
            addItem.setRecommend(restaurant.getRecommend());
            addItem.setAddress(restaurant.getAddress());

            result.add(addItem);
        }
        return result;
    }

    public RestaurantResponse getRestaurant(long id){
        Restaurant originData = restaurantRepository.findById(id).orElseThrow();
        RestaurantResponse response = new RestaurantResponse();
        response.setId(originData.getId());
        response.setMemberId(originData.getMember().getId());
        response.setMemberName(originData.getMember().getName());
        response.setRecommend(originData.getRecommend());
        response.setAddress(originData.getAddress());

        return response;
    }

    public void putRestaurant(long id, RestaurantInfoChangeRequest request){
        Restaurant originData = restaurantRepository.findById(id).orElseThrow();
        originData.setAddress(request.getAddress());
        originData.setRestaurantNumber(request.getRestaurantNumber());
        originData.setEtcMemo(request.getEtcMemo());
        restaurantRepository.save(originData);
    }

    public void delRestaurant(long id){
        restaurantRepository.deleteById(id);
    }
}

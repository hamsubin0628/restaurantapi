package org.hsb.restaurantapi.service;

import lombok.RequiredArgsConstructor;
import org.hsb.restaurantapi.entity.Member;
import org.hsb.restaurantapi.model.member.MemberItem;
import org.hsb.restaurantapi.model.member.MemberCreateRequest;
import org.hsb.restaurantapi.model.member.MemberResponse;
import org.hsb.restaurantapi.repository.MemberRepository;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id){
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request){
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setDues(request.getDues());

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers(){
        List<Member> originList = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();

        for (Member member : originList){
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setName(member.getName());
            addItem.setPhoneNumber(member.getPhoneNumber());

            result.add(addItem);
        }
        return result;
    }

    public MemberResponse getMember(long id){
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setIsDues(originData.getDues() ? "납부함":"납부안함");

        return response;
    }
}

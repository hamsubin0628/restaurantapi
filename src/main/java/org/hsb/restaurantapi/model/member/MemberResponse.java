package org.hsb.restaurantapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberResponse {
    private Long id;
    private String name;
    private String isDues;
}

package org.hsb.restaurantapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberCreateRequest {
    private String name;
    private String phoneNumber;
    private Boolean dues;
}

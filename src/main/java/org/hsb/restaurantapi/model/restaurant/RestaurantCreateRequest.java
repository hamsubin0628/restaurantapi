package org.hsb.restaurantapi.model.restaurant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestaurantCreateRequest {
    private Long memberId;
    private String recommend;
    private String address;
    private String restaurantNumber;
    private String etcMemo;
}

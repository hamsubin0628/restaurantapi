package org.hsb.restaurantapi.model.restaurant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestaurantItem {
    private Long id;
    private Long memberId;
    private String recommend;
    private String address;
}

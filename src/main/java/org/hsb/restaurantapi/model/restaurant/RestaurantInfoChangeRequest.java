package org.hsb.restaurantapi.model.restaurant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestaurantInfoChangeRequest {
    private String address;
    private String restaurantNumber;
    private String etcMemo;
}

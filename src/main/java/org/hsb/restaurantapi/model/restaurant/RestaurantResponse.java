package org.hsb.restaurantapi.model.restaurant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestaurantResponse {
    private Long id;
    private Long memberId;
    private String memberName;
    private String recommend;
    private String address;
}

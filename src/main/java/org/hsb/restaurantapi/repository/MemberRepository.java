package org.hsb.restaurantapi.repository;

import org.hsb.restaurantapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}

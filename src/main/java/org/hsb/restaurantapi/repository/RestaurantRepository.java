package org.hsb.restaurantapi.repository;

import org.hsb.restaurantapi.entity.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {
}
